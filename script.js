var i = 0;

$(document).on('click', '.slider-next', function(){
    var next = i + 10;
    if (i === 70) {
      $('div.scroll-wrapper ul.row li').removeAttr('style');
      $('div.scroll-wrapper ul.row li').attr('style', 'left:0%;-webkit-transition:left 1s;transition:left 1s;');
      i = 0;
    } else {
      $('div.scroll-wrapper ul.row li').removeAttr('style');
      $('div.scroll-wrapper ul.row li').attr('style', 'left:-'+ next +'.1%;-webkit-transition:left 1s;transition:left 1s;');
      i = next;
    }   
});

$(document).on('click', '.slider-previous', function(){
    var previous = i - 10;
    if (i === 0) {
        $('div.scroll-wrapper ul.row li').removeAttr('style');
        $('div.scroll-wrapper ul.row li').attr('style', 'left:-70%;-webkit-transition:left 1s;transition:left 1s;');
        i = 70;
      } else {
        $('div.scroll-wrapper ul.row li').removeAttr('style');
        $('div.scroll-wrapper ul.row li').attr('style', 'left:-'+ previous +'.1%;-webkit-transition:left 1s;transition:left 1s;');
        i = previous;
      }
});


// ======back to top =================
//Get the button
let mybutton = document.getElementById("btn-back-to-top");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (
    document.body.scrollTop > 5 ||
    document.documentElement.scrollTop > 5
  ) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}
// When the user clicks on the button, scroll to the top of the document
mybutton.addEventListener("click", backToTop);

function backToTop() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}


